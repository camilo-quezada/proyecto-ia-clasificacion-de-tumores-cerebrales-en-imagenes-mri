# Proyecto IA Clasificación de Tumores Cerebrales en imágenes MRI



**Autores: Juan Diego Calderón Carrillo, Daniel Alejandro León Tarazona, Efrain Camilo Quezada Sanchez**

<img src="Data/Logo.jpeg" alt="Banner_proyecto"/>

**Objetivo:**
Ayudar a pronosticar y clasificar tumores cerebrales

- Dataset: Brain Tumor Classification MRI (Kaggle)
- Modelo:Aprendizaje supervisado,Naive Bayes,DTC,RFC,SVM; Red neuronal convolucional, ResNet50 

[(Code)](https://gitlab.com/camilo-quezada/proyecto-ia-clasificacion-de-tumores-cerebrales-en-imagenes-mri/-/blob/main/Data/ProyectoIA1.ipynb)[(Video)](https://www.youtube.com/watch?v=7g15EKgbXPA)[(+Info)](https://gitlab.com/camilo-quezada/proyecto-ia-clasificacion-de-tumores-cerebrales-en-imagenes-mri/-/raw/main/Data/IA1_MRI.pdf?)

